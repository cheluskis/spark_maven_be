
from flask import Flask


def create_app():
    # instantiate the app
    app = Flask(__name__)

    # register blueprints
    from scheduler.api.api import api_blueprint
    app.register_blueprint(api_blueprint)

    return app


app = create_app()
