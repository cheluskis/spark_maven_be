
import json
from datetime import timedelta

import dateutil.parser
from flask import Blueprint, jsonify, request

api_blueprint = Blueprint('api', __name__)
db = {'1': ["2021-11-17 10:29:48.802627", "2021-11-17 11:48:48.802627"]}


@api_blueprint.route('/ping', methods=['GET'])
def ping_pong():
    return {
        'status': 'success',
        'message': 'pong!'
    }


def overlap(choosen_dt, x):
    res = False
    x_dt = dateutil.parser.parse(x)
    if x_dt <= choosen_dt <= x_dt + timedelta(minutes=30):
        # intefieres with x_dtt appointment
        res = True

    return res


@api_blueprint.route('/api/set', methods=['POST'])
def create_appointment():

    user_id = request.json.get('user_id', None)
    datetime_choosen = request.json.get('datetime_choosen', "")

    try:
        choosen_dt = dateutil.parser.parse(datetime_choosen)
    except (ValueError, OverflowError) as e:  # OR except Exception as e:
        print(f'Error: parsing date on details {datetime_choosen} {e}')
        response_object = {
            'status': 'fail',
            'message': 'datetime_choosen does is not correct format'
        }
        return jsonify(response_object), 400

    if user_id in db.keys():
        overlaps = [x for x in db[user_id] if overlap(choosen_dt, x)]
        if len(overlaps) == 0:
            response_object = {
                'status': 'success',
                'message': 'created'
            }
            db[user_id].append(str(choosen_dt))
            return jsonify(response_object), 201
        else:
            response_object = {
                'status': 'fail',
                'message': 'user is not available'
            }
            return jsonify(response_object), 400

    else:
        response_object = {
            'status': 'fail',
            'message': 'user does not exist'
        }
        return jsonify(response_object), 404


@api_blueprint.route('/api/<user_id>', methods=['GET'])
def get_user_appointments(user_id):
    """Get single user details"""
    response_object = {
        'status': 'fail',
        'message': 'User does not exist'
    }
    if user_id and user_id in db.keys():
        response_object = {
            'status': 'success',
            'appointments': json.dumps(db[user_id])
        }
        return jsonify(response_object), 200
    else:
        return jsonify(response_object), 400
