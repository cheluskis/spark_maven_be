# README #

Steps are necessary to get your application up and running.

### What is this repository for? ###

* Contains Appointments REST API
* Version 1
* git@bitbucket.org:cheluskis/spark_maven_be.git

## Characteristics and Features

- Done in Flask
- No database needed (persistence is in memory while running the server)
- Demo Postman Collection attached to try endpoints

## Installation

Appointments API requires [Python](https://www.python.org/downloads/) v3.5+ to run
Appointments API requires git to clone the repo

Install the dependencies and start the server.

```sh
git clone git@bitbucket.org:cheluskis/spark_maven_be.git
cd spark_maven_be
pip install -r requirements.txt
python3 manage.py runserver -h 0.0.0.0
```



## Endpoints

| Endpoint | HTTP Method | Result |
|:---|:---:|---|
| `/api/<user_id>`  | `GET`  | Get appointements for this user_id (integer)  |
| `/api/set`  | `POST`  | Set an appointement: required fields *user_id* (string number) and *choosen_datetime* (iso format datetime)


### Usage Examples ###

##### GET APPOINTMENTS #####

```sh
curl --location --request GET 'localhost:5000/api/1'
```

##### POST APPOINTMENTS #####

```sh
curl --location --request POST 'localhost:5000/api/set' \
--header 'Content-Type: application/json' \
--data-raw '{
    "datetime_choosen": "2021-11-17 11:00:00.802627",
    "user_id": "1",
    "password": "123456"
}'
```

## Contributions

- Lint code with Flake8

### TODOs ###
### TODOs ###
- Make more consistent naming
- Add tests, pytest recommended
- Add Circle CI or similar
- Remove and factorize repeated response objects
- Dockerize
- better url names like /appointments
- use a typical for or a generator instead of a list comprehension to get the first overlap (perfomance)
- all appointments must start and end on the hour or half hour is NOT restricted (but I think is more flexible)